<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="room" content="UIN Arraniry Room">
  <meta name="author" content="myroom">
  <title>Myroom Ar-Raniry</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/lightbox.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link id="css-preset" href="css/presets/preset1.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <!--<link rel="shortcut icon" href="img/<?php echo $setting["favico"]
                                    ?>">-->
    <link rel="stylesheet" href="css/jquery.orgchart.min.css">
    
    <link href="css/inovasicaption.css" rel="stylesheet">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <!-- fullCalendar -->
  <link rel="stylesheet" href="dist/fullcalendar.min.css">
  <link rel="stylesheet" href="dist/fullcalendar.print.min.css" media="print">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
</head>