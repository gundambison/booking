-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 24, 2018 at 11:31 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fb_ruang`
--

-- --------------------------------------------------------

--
-- Table structure for table `pemesan_ruangan`
--

CREATE TABLE IF NOT EXISTS `pemesan_ruangan` (
  `id` int(5) NOT NULL,
  `kode_ruangan` varchar(25) NOT NULL,
  `nama_pemesan` varchar(25) NOT NULL,
  `nama_ruangan` varchar(25) NOT NULL,
  `prodi_dosen` varchar(25) NOT NULL,
  `jam_mulai` varchar(25) NOT NULL,
  `durasi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ruang_b_tersedia`
--

CREATE TABLE IF NOT EXISTS `ruang_b_tersedia` (
  `id_ruangan` smallint(4) unsigned zerofill NOT NULL,
  `kode_ruangan` varchar(20) DEFAULT '-',
  `nama_ruangan` varchar(50) NOT NULL,
  `ket_ruangan` varchar(200) DEFAULT '-',
  `id_grup_ruangan` smallint(2) unsigned zerofill NOT NULL,
  `id_lokasi` mediumint(2) DEFAULT NULL COMMENT 'Link ke tbl_mst_fakultas, untuk menandai dimana lokasi ruangan ini, khususnya ruang belajar.',
  `kapasitas` decimal(3,0) DEFAULT '0' COMMENT 'Kapasitas Daya Tampung Ruangan (Orang)',
  `user_created` varchar(35) DEFAULT NULL COMMENT 'User yang menciptakan ruangan ini',
  `date_created` datetime DEFAULT NULL COMMENT 'Tanggal diciptakan ruangan ini',
  `user_modified` varchar(35) DEFAULT NULL COMMENT 'User yang mengedit data ruangan ini',
  `date_modified` datetime DEFAULT NULL COMMENT 'Tanggal dieditnya data ruangan ini',
  `isdefault` smallint(1) unsigned DEFAULT '0' COMMENT '1 = Default, 0 = No',
  `isactive` smallint(1) unsigned DEFAULT '1' COMMENT '1 = Active, 0 = No'
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang_b_tersedia`
--

INSERT INTO `ruang_b_tersedia` (`id_ruangan`, `kode_ruangan`, `nama_ruangan`, `ket_ruangan`, `id_grup_ruangan`, `id_lokasi`, `kapasitas`, `user_created`, `date_created`, `user_modified`, `date_modified`, `isdefault`, `isactive`) VALUES
(0001, 'UIN 103 01', 'FTK 01 Gedung B', '-', 11, 3, '40', NULL, NULL, 'admin-ebak', '2018-03-27 22:30:03', 0, 1),
(0002, 'UIN 103 02', 'FTK 02 Gedung B', '-', 01, NULL, '28', NULL, NULL, NULL, NULL, 0, 1),
(0003, 'UIN 103 03', 'FTK 03 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0004, 'UIN 103 04', 'FTK 04 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0005, 'UIN 103 05', 'FTK 05 Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0006, 'UIN 103 06', 'FTK 06 Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0007, 'UIN 103 07', 'FTK 07 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0008, 'UIN 103 08', 'FTK 08 Gedung B', '-', 01, NULL, '28', NULL, NULL, NULL, NULL, 0, 1),
(0009, 'UIN 103 09', 'FTK 09 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0010, 'UIN 103 10', 'FTK 10 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0011, 'UIN 103 11', 'FTK 11 Gedung B', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0012, 'UIN 103 12', 'FTK 12 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0013, 'UIN 103 13', 'FTK 13 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0014, 'UIN 103 14', 'FTK 14 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0015, 'UIN 103 15', 'FTK 15 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0016, 'UIN 103 16', 'FTK 16 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0017, 'UIN 103 17', 'FTK 17 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0018, 'UIN 103 18', 'FTK 18 Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0019, 'UIN 103 19', 'FTK 19 Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0020, 'UIN 103 20', 'FTK 20 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0021, 'UIN 103 21', 'FTK 21 Gedung B', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0022, 'UIN 103 22', 'FTK 22 Gedung B', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0023, 'UIN 102 01', 'FTK 23 B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0024, 'UIN 102 02', 'FTK 24 B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0025, 'UIN 102 03', 'FTK 25 B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0026, 'UIN 091 01', 'FTK 26 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0027, 'UIN 091 02', 'FTK 27 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0028, 'UIN 091 03', 'FTK 28 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0029, 'UIN 091 04', 'FTK 29 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0030, 'UIN 091 05', 'FTK 30 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0031, 'UIN 091 06', 'FTK 31 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0032, 'UIN 091 07', 'FTK 32 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0033, 'UIN 091 08', 'FTK 33 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0034, 'UIN 091 09', 'FTK 34 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0035, 'UIN 091 10', 'FTK 35 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0036, 'UIN 091 11', 'FTK 36 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0037, 'UIN 091 12', 'FTK 37 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0038, 'UIN 092 01', 'FTK 38 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0039, 'UIN 092 02', 'FTK 39 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0040, 'UIN 092 03', 'FTK 40 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0041, 'UIN 092 04', 'FTK 41 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0042, 'UIN 092 05', 'FTK 42 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0043, 'UIN 092 06', 'FTK 43 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0044, 'UIN 092 07', 'FTK 44 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0045, 'UIN 092 08', 'FTK 45 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0046, 'UIN 092 09', 'FTK 46 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0047, 'UIN 092 10', 'FTK 47 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0048, 'UIN 092 11', 'FTK 48 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0049, 'UIN 092 12', 'FTK 49 A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0050, 'UIN 092 13', 'FTK 50 A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0051, 'UIN 092 14', 'FTK 51 A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0052, 'UIN 092 15', 'FTK 52 A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0053, 'UIN 111 01', 'FTK 53 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0054, 'UIN 111 02', 'FTK 54 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0055, 'UIN 111 03', 'FTK 55 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0056, 'UIN 111 04', 'FTK 56 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0057, 'UIN 121 01', 'Ushuluddin-01', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0058, 'UIN 121 02', 'Ushuluddin-02', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0059, 'UIN 121 03', 'Ushuluddin-03', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0060, 'UIN 122 01', 'Ushuluddin-07', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0061, 'UIN 122 02', 'Ushuluddin-08', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0062, 'UIN 122 03', 'Ushuluddin-09', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0063, 'UIN 121 04', 'Ushuluddin-04', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0064, 'UIN 121 05', 'Ushuluddin-05', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0065, 'UIN 121 06', 'Ushuluddin-06', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0066, 'UIN 122 04', 'Ushuluddin-10', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0067, 'UIN 122 05', 'Ushuluddin-11', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0068, 'UIN 122 06', 'Ushuluddin-12', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0069, 'UIN 121 07', 'FISIP', '-', 01, NULL, '50', NULL, NULL, NULL, NULL, 0, 1),
(0070, 'UIN 121 08', 'FISIP', '-', 01, NULL, '50', NULL, NULL, NULL, NULL, 0, 1),
(0071, 'UIN 023 01', 'FAH - 001', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0072, 'UIN 023 02', 'FAH - 002', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0073, 'UIN 023 03', 'FAH - 003', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0074, 'UIN 023 04', 'FAH - 004', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0075, 'UIN 022 03', 'FAH - 12 LK', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0076, 'UIN 023 05', 'FAH - 005', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0077, 'UIN 023 06', 'FAH - 006', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0078, 'UIN 023 07', 'FAH - 007', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0079, 'UIN 023 08', 'FAH - 008', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0080, 'UIN 023 09', 'FAH - 009', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0081, 'UIN 022 01', 'FAH 10', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0082, 'UIN 022 02', 'FAH - 11', '-', 01, NULL, '42', NULL, NULL, NULL, NULL, 0, 1),
(0083, 'UIN 021 01', 'FAH-13', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0084, 'UIN 022 09', 'FAH - 14', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0085, 'UIN 022 08', 'FAH - 15', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0086, 'UIN 022 07', 'FAH - 16', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0087, 'UIN 223 04', 'FEBI-30 Psikologi New Building', 'FEBI-30 Psikologi New Building', 11, 10, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:37:04', 0, 1),
(0088, 'UIN 213 11', 'FEBI-20 New Building', 'FEBI-20 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:32:32', 0, 1),
(0089, 'UIN 223 02', 'FEBI-28 Psikologi New Building', 'FEBI-28 Psikologi New Building', 11, 10, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:24:05', 0, 1),
(0090, 'UIN 223 03', 'FEBI-29 Psikologi New Building', 'FEBI-29 Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 02:15:39', 0, 1),
(0091, 'UIN 112 01', 'FTK 57 C', '-', 01, NULL, '12', NULL, NULL, NULL, NULL, 0, 1),
(0092, 'UIN 112 02', 'FTK 58 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0093, 'UIN 112 03', 'FTK 59 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0094, 'UIN 112 04', 'FTK 60 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0095, 'UIN 112 05', 'FTK 61 C', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0096, 'UIN 022 06', 'FTK 68-Adab Baru', 'Pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:04:44', 0, 1),
(0097, 'UIN 021 03', 'FTK 62-Adab', 'Pengganti FTK 63 Ushuluddin', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 18:58:52', 0, 1),
(0098, 'UIN 311 01', 'PFS-Optik', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0099, 'ZZN 152 02', 'Audit-PSW', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0100, 'UIN 021 04', 'FTK 63-Adab', 'Pengganti FTK-64 Ushuluddin', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:01:46', 0, 1),
(0101, 'UIN 132 03', 'LDC-Lab Bahasa-PBI', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0102, 'ZZN 152 06', 'Gudang', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0103, 'ZLab-PMA01', 'Lab. Komputer PMA-01', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0104, 'ZZN 152 07', 'Auditorium-Gudang', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0105, 'UIN 071 03', 'FSH Hasbi', 'Ganti 181 01', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-17 03:17:01', 0, 1),
(0106, 'UIN 321 01', 'FTK- (Lab.Kimia)', '-laboratorium PKM', 11, 3, '35', NULL, NULL, 'admin-ebak', '2018-02-08 23:28:54', 0, 1),
(0107, 'ZZLDC 02', 'PUSAT BAHASA', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0108, 'UIN 131 01', 'FISIP', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0109, 'UIN 131 02', 'FISIP', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0110, 'UIN 132 01', 'FISIP', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0111, 'UIN 132 02', 'FISIP', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0112, 'ZZLDC 07', 'PUSAT BAHASA', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0113, 'ZZLDC 08', 'PUSAT BAHASA', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0114, 'UIN 081 05', 'FDK-04', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0115, 'UIN 081 06', 'FDK-05', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0116, 'UIN 081 07', 'FDK-06', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0117, 'UIN 081 08', 'FDK-07', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0118, 'ZZFDK 009', 'Gedung Fak. Dakwah', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0119, 'UIN 082 02', 'FDK-12', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0120, 'UIN 082 03', 'FDK-13', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0121, 'UIN 082 04', 'FDK-14', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0122, 'UIN 082 05', 'FDK-15', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0123, 'UIN 082 06', 'FDK-16', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0124, 'UIN 082 07', 'FDK-17', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0125, 'UIN 082 08', 'FDK-18', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0126, 'UIN 082 09', 'FDK-19', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0127, 'UIN 081 09', 'FDK-08', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0128, 'ZZFDK 020', 'Gedung Fak. Dakwah', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0129, 'UIN 213 18', 'FEBI-27 New Building', 'FEBI-27 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:35:33', 0, 1),
(0130, 'UIN 213 13', 'FEBI-22 New Building', 'FEBI-22 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:20:46', 0, 1),
(0131, 'UIN 081 12', 'FDK-11', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0132, 'UIN 081 16', 'FDK-Theater FDK', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0133, 'ZZFDK 001', 'Gedung Fak. Dakwah', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0134, 'UIN 081 02', 'FDK-01', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0135, 'UIN 081 03', 'FDK-02', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0136, 'UIN 081 04', 'FDK-03', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0137, 'UIN 033 11', 'FSH Gedung A', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0138, 'UIN 061 02', 'FSH Gedung CINA', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0139, 'UIN 072 02', 'FSH Hasbi', 'Ganti Ruangan UIN 081 14', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-24 19:15:43', 0, 1),
(0140, 'UIN 051 01', 'FSH Gedung C', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0141, 'UIN 041 01', 'FSH Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0142, 'UIN 071 01', 'FSH Hasbi', 'Ganti 041 02', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-18 23:23:25', 0, 1),
(0143, 'UIN 052 01', 'FSH Gedung C', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0144, 'UIN 052 02', 'FSH Gedung C', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0145, 'UIN 033 01', 'FSH Gedung A', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0146, 'UIN 033 02', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0147, 'UIN 033 03', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0148, 'UIN 033 04', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0149, 'UIN 033 05', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0150, 'UIN 033 06', 'FSH Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0151, 'UIN 033 07', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0152, 'UIN 033 08', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0153, 'UIN 122 10', 'Ushuluddin-13', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0154, 'ZZFSH 00', 'Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0155, 'UIN 033 09', 'FSH Gedung A', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0156, 'UIN 033 10', 'FSH Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0157, 'UIN 211 09', 'FEBI-09 New Building', 'FEBI-09 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 01:14:48', 0, 1),
(0158, 'UIN 211 08', 'FEBI-08 New Building', 'FEBI-08 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 01:13:45', 0, 1),
(0159, 'UIN 213 01', 'FEBI-10 New Building', 'FEBI-10 New Building', 11, 7, '40', NULL, NULL, 'admin-ebak', '2018-02-17 02:16:57', 0, 1),
(0160, 'UIN 213 02', 'FEBI-11 New Building', 'FEBI-11 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:07:53', 0, 1),
(0161, 'UIN 213 03', 'FEBI-12 New Building', 'FEBI-12 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:29:37', 0, 1),
(0162, 'UIN 213 04', 'FEBI-13 New Building', 'FEBI-13 New Building', 11, 7, '40', NULL, NULL, 'admin-ebak', '2018-02-17 02:25:29', 0, 1),
(0163, 'UIN 213 05', 'FEBI-14 New Building', 'FEBI-14 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:17:59', 0, 1),
(0164, 'UIN 213 06', 'FEBI-15 New Building', 'FEBI-15 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:09:01', 0, 1),
(0165, 'UIN 213 07', 'FEBI-16 New Building', 'FEBI-16 New Building', 11, 7, '25', NULL, NULL, 'admin-ebak', '2018-02-17 02:31:08', 0, 1),
(0166, 'UIN 213 08', 'FEBI-17 New Building', 'FEBI-17 New Building', 11, 7, '25', NULL, NULL, 'admin-ebak', '2018-02-17 02:27:05', 0, 1),
(0167, 'UIN 213 09', 'FEBI-18 New Building', 'FEBI-18 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:19:04', 0, 1),
(0168, 'UIN 213 10', 'FEBI-19 New Building', 'FEBI-19 New Building', 11, 8, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:10:24', 0, 1),
(0169, 'UIN 051 02', 'FSH Gedung C', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0170, 'ZZFSH 06', 'Gedung C', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0171, 'UIN 042 02', 'FSH Gedung B Lt. 2', 'ganti UIN 033 12', 11, 2, '40', NULL, NULL, 'admin-ebak', '2018-02-21 15:48:05', 0, 1),
(0172, 'UIN 032 01', 'FSH Gedung B', '', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-21 15:46:42', 0, 1),
(0173, 'UIN 061 01', 'FSH Gedung CINA', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0174, 'UIN 072 01', 'FSH Hasbi', 'Ganti Ruangan UIN 081 13', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-24 19:15:30', 0, 1),
(0175, 'ZZLDC 09', 'PUSAT BAHASA', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0176, 'ZZLDC 10', 'PUSAT BAHASA', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0177, 'UIN 081 10', 'FDK-09', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0178, 'UIN 223 05', 'FEBI-31 Psikologi New Building', 'FEBI-31 Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 14:48:49', 0, 1),
(0179, 'ZZFAH 013B', 'Gedung FAH Baru', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0180, 'ZZFAH 014B', 'Gedung FAH Baru', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0181, 'ZZFAH 015B', 'Gedung FAH Baru', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0182, 'ZZFAH 016B', 'Gedung FAH Baru', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0183, 'UIN 021 02', 'FAH - 17 lantai 1', 'FAH - 17 lantai 1', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-26 11:29:04', 0, 1),
(0184, 'UIN 023 10', 'FAH - 18', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0185, 'ZZFAH 017B', 'Gedung FAH Baru', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0186, 'UIN 071 02', 'FSH Hasbi', 'ganti 041 03', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-17 03:15:55', 0, 1),
(0187, 'UIN 042 01', 'FSH Gedung B', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0188, 'Z 000 05', 'FEBI-Bayangan D3', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0189, 'UIN 012 14', 'UIN 012 14', 'FST-14', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 22:45:53', 0, 1),
(0190, 'ZZN 000 07', 'Bayangan', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0191, 'ZZN 000 08', 'Bayangan', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0192, 'ZZN 000 09', 'Bayangan', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0193, 'ZZN 000 10', 'Bayangan', '-', 01, NULL, '45', NULL, NULL, NULL, NULL, 0, 1),
(0194, 'UIN 071 04', 'FSH Hasbi', 'Ganti 181 02', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-17 03:17:45', 0, 1),
(0195, 'UIN 071 05', 'FSH Hasbi', 'Ganti 181 03', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-17 03:18:38', 0, 1),
(0196, 'UIN 021 05', 'FTK 64-Adab', 'Pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:01:55', 0, 1),
(0197, 'UIN 022 04', 'FTK 66-Adab baru', 'Pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:03:01', 0, 1),
(0198, 'UIN 021 06', 'FTK 65-Adab', 'pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:02:08', 0, 1),
(0199, 'ZZFAH 011B', 'Gedung FAH Lt.2', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0200, 'ZZDInn071', 'Darussalam Inn 071', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0201, 'ZZDInn072', 'Darussalam Inn 072', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0202, 'ZZDInn07', 'Darussalam Inn 073', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0203, 'ZZADT070', 'Auditorium070', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0204, 'UIN 331 01', 'Bayangan-PTI', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0205, 'UIN 092 16', 'FTK A Lab-PGMI', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0206, 'UIN 081 15', 'FDK-LAB Komputer', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0207, 'UIN 213 14', 'FEBI-23 New Building', 'FEBI-23 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:11:50', 0, 1),
(0208, 'UIN 213 15', 'FEBI-24 New Building', 'FEBI-24 New Building', 11, 7, '25', NULL, NULL, 'admin-ebak', '2018-02-17 02:33:56', 0, 1),
(0209, 'UIN 213 12', 'FEBI-21 New Building', 'FEBI-21 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:28:14', 0, 1),
(0210, 'UIN 213 16', 'FEBI-25 New Building', 'FEBI-25 New Building', 11, 7, '25', NULL, NULL, 'admin-ebak', '2018-02-17 02:22:14', 0, 1),
(0211, 'UIN 213 17', 'FEBI-26 New Building', 'FEBI-26 New Building', 11, 7, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:13:23', 0, 1),
(0212, 'UIN 013 01', 'FST', 'FST', 11, 8, '35', NULL, NULL, 'admin-ebak', '2018-02-17 02:39:09', 0, 1),
(0213, 'UIN 081 11', 'FDK-10', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0214, 'ZZFSH 26', 'Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0215, 'ZZFSH 27', 'Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0216, 'ZZFSH 28', 'Gedung A', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0217, 'UIN 013 04', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:40:57', 0, 1),
(0218, 'UIN 013 05', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:41:17', 0, 1),
(0219, 'UIN 013 06', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:42:48', 0, 1),
(0220, 'UIN 013 09', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:41:34', 0, 1),
(0221, 'ZZFEBI 27', 'Lt. 3 Gedung Saintek', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0222, 'ZZFEBI 28', 'Lt. 3 Gedung Saintek', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0223, 'ZZFEBI 29', 'Lt. 3 Gedung Saintek', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0224, 'ZZFEBI 30', 'Lt. 3 Gedung Saintek', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0225, 'ZZPSI-1', 'PSI-1', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0226, 'UIN 082 12', 'FDK-LAB PMI', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0227, 'UIN 082 11', 'FDK-LAB BKI', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0228, 'UIN 082 10', 'FDK-Ruang Sidang II', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0229, 'UIN 022 05', 'FTK 67-Adab baru', 'Pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:04:14', 0, 1),
(0230, 'UIN 023 13', 'FTK 70-Adab', 'Pengganti FTK-Audit', 11, 6, '40', NULL, NULL, 'admin-ebak', '2018-02-24 19:13:14', 0, 1),
(0231, 'UIN 023 12', 'FTK 69-Adab', 'Pengganti FTK-Audit', 11, 6, '37', NULL, NULL, 'admin-ebak', '2018-02-24 19:08:33', 0, 1),
(0232, 'ZZPUSKOM01', 'LAB 01', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0233, 'ZZPUSKOM02', 'LAB 02', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0234, 'ZLab-PMA02', 'Lab. Komputer PMA-02', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0235, 'UIN 222 01', '01-Psikologi New Building', '01-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:14:20', 0, 1),
(0236, 'UIN 222 02', '02-Psikologi New Building', '02-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:14:31', 0, 1),
(0237, 'UIN 222 03', '03-Psikologi New Building', '03-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:14:40', 0, 1),
(0238, 'UIN 222 04', '04-Psikologi New Building', '04-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:14:50', 0, 1),
(0239, 'UIN 222 05', '05-Psikologi New Building', '05-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:15:00', 0, 1),
(0240, 'UIN 222 06', '06-Psikologi New Building', '06-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 15:15:11', 0, 1),
(0241, 'UIN 013 02', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:40:07', 0, 1),
(0242, 'UIN 013 03', 'FST', 'FST', 11, 8, '30', NULL, NULL, 'admin-ebak', '2018-02-17 02:40:32', 0, 1),
(0243, 'UIN 012 01', 'FST-Abu Bakar', 'FST-Abu Bakar', 11, 9, '35', NULL, NULL, 'adan', '2018-02-03 23:22:53', 0, 1),
(0244, 'UIN 012 02', 'FST-Umar bin Khattab', 'FST-Umar bin Khattab', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:23:04', 0, 1),
(0245, 'UIN 012 03', 'FST-Usman bin Affan', 'FST-Usman bin Affan', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:23:18', 0, 1),
(0246, 'UIN 012 04', 'FST-Ali', 'FST-Ali ', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:23:43', 0, 1),
(0247, 'UIN 012 05', 'FST-Ibnu Sina', 'FST-Ibnu Sina', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:23:55', 0, 1),
(0248, 'UIN 012 06', 'FST-Al-Faraby', 'FST-Al-Faraby', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:24:05', 0, 1),
(0249, 'UIN 012 07', 'FST-Al-Jabar', 'FST-Al-Jabar', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:24:16', 0, 1),
(0250, 'UIN 012 08', 'FST-Ibnu Khaldun', 'FST-Ibnu Khaldun', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:24:24', 0, 1),
(0251, 'UIN 012 09', 'FST-Al-Kindi', 'FST-Al-Kindi', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:24:33', 0, 1),
(0252, 'UIN 012 10', 'FST-Al-Ghazali', 'FST-Al-Ghazali', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 23:24:46', 0, 1),
(0253, 'UIN 012 11', 'UIN 012 11', 'FST-Ibnu Rusydi', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 22:44:19', 0, 1),
(0254, 'UIN 013 08', 'FST-Al-Khazimi', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0255, 'UIN 013 07', 'FST-Al-Khawarizmi', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0256, 'UIN 012 12', 'UIN 012 12', 'FST-Al-Jazab', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 22:44:59', 0, 1),
(0257, 'UIN 012 13', 'UIN 012 13', 'FST-13', 11, 8, '35', NULL, NULL, 'adan', '2018-02-03 22:45:27', 0, 1),
(0258, 'UIN 013 10', 'FST-Studio 1', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0259, 'UIN 013 11', 'FST-Studio 2', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0260, 'UIN 013 12', 'FST-Studio 3', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0261, 'UIN 013 13', 'FST-Studio 4', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0263, 'UIN 071 06', 'FSH Hasbi', 'Ganti 191 01', 11, 2, '35', NULL, NULL, 'admin-ebak', '2018-02-17 03:19:35', 0, 1),
(0264, 'ZZFTK 779', 'Lab. BK', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0265, 'UIN 102 04', 'FTK-LAB-PBI', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0266, 'ZZFTK-LAB', 'LAB-PBA', '-', 01, NULL, '40', NULL, NULL, NULL, NULL, 0, 1),
(0267, 'ZZN 141 01', 'FEBI-Pustaka', '-', 01, NULL, '35', NULL, NULL, NULL, NULL, 0, 1),
(0268, 'ZZFTK-MTK', 'LAB Peraga MTK', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0269, 'UIN 223 01', '07-Psikologi New Building', '07-Psikologi New Building', 11, 10, '40', NULL, NULL, 'admin-ebak', '2018-02-17 14:46:52', 0, 1),
(0270, 'ZZICT001', 'Lab Kom I', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0271, 'ZZICT002', 'Lab Kom II', '-', 01, NULL, '20', NULL, NULL, NULL, NULL, 0, 1),
(0272, 'UIN 122 09', 'Seminar FISIP', '-', 01, NULL, '25', NULL, NULL, NULL, NULL, 0, 1),
(0273, 'UIN 152 11', 'FDK-Audit', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0274, 'UIN 152 12', 'FDK-Audit', '-', 01, NULL, '30', NULL, NULL, NULL, NULL, 0, 1),
(0289, 'UIN 211 01', 'FEBI-01 New Building', 'FEBI-01 New Building', 11, 7, '30', 'adan', '2018-02-05 10:05:42', NULL, NULL, 0, 1),
(0290, 'UIN 211 02', 'FEBI-02 New Building', 'FEBI-02 New Building', 11, 7, '25', 'adan', '2018-02-05 11:22:59', 'admin-ebak', '2018-02-17 01:02:43', 0, 1),
(0291, 'UIN 211 03', 'FEBI-03 New Building', 'FEBI-03 New Building', 11, 7, '25', 'adan', '2018-02-05 11:31:12', 'admin-ebak', '2018-02-17 01:05:21', 0, 1),
(0292, 'UIN 211 04', 'FEBI-04 New Building', 'FEBI-04 New Building', 11, 7, '35', 'adan', '2018-02-05 11:31:53', 'admin-ebak', '2018-02-17 01:06:53', 0, 1),
(0293, 'UIN 211 05', 'FEBI-05 New Building', 'FEBI-05 New Building', 11, 7, '35', 'adan', '2018-02-05 11:33:22', 'admin-ebak', '2018-02-17 01:07:47', 0, 1),
(0294, 'UIN 211 06', 'FEBI-06 New Building', 'FEBI-06 New Building', 11, 7, '35', 'adan', '2018-02-05 11:33:51', 'admin-ebak', '2018-02-17 01:08:06', 0, 1),
(0295, 'UIN 211 07', 'FEBI-07 New Building', 'FEBI-07 New Building', 11, 7, '35', 'adan', '2018-02-05 11:34:38', 'admin-ebak', '2018-02-17 01:09:02', 0, 1),
(0296, 'UIN 321 02', 'FTK- (Lab.Kimia)', 'laboratorium PKM', 11, 3, '35', 'admin-ebak', '2018-02-08 23:30:29', NULL, NULL, 0, 1),
(0297, 'UIN 999 98', 'PTE-Bayangan', 'Ruangan sementara PTE', 11, 3, '40', 'admin-ebak', '2018-02-16 20:53:14', NULL, NULL, 0, 1),
(0300, 'UIN 223 06', 'FEBI-32 Psikologi New Building', 'FEBI-32 Psikologi New Building', 11, 10, '40', 'admin-ebak', '2018-02-17 14:57:13', NULL, NULL, 0, 1),
(0302, 'UIN 023 11', 'FTK 72 - Adab', 'Ruangan Baru FTK - Adab', 11, 6, '40', 'admin-ebak', '2018-02-22 16:32:17', 'admin-ebak', '2018-02-26 11:31:11', 0, 1),
(0303, 'UIN 023 14', 'FTK 71 - Adab', 'Ruangan Baru FTK - Adab', 11, 6, '40', 'admin-ebak', '2018-02-22 16:32:53', 'admin-ebak', '2018-02-24 19:09:57', 0, 1),
(0304, 'UIN 122 07', 'Ushuluddin-14', 'Ushuluddin-14 (kosong)', 11, 4, '35', 'admin-ebak', '2018-02-22 16:37:50', NULL, NULL, 0, 1),
(0305, 'UIN 122 08', 'Ushuluddin-14', 'Ushuluddin-14 (kosong)', 11, 4, '35', 'admin-ebak', '2018-02-22 16:38:32', NULL, NULL, 0, 1),
(0306, 'UIN 081 13', 'FDK-20', 'FDK-20 (kosong)', 11, 5, '35', 'admin-ebak', '2018-02-22 16:41:12', NULL, NULL, 0, 1),
(0307, 'UIN 081 14', 'FDK-21', 'FDK-21 (kosong)', 11, 5, '35', 'admin-ebak', '2018-02-22 16:41:37', NULL, NULL, 0, 1),
(0309, 'UIN 071 07', 'FSH_hasbi (dekat TK)', 'FSH_hasbi (dekat TK)', 11, 2, '40', 'admin-ebak', '2018-02-24 19:16:38', NULL, NULL, 0, 1),
(0310, 'UIN 071 08', 'FSH_hasbi (dekat TK)', 'FSH_hasbi (dekat TK)', 11, 2, '40', 'admin-ebak', '2018-02-24 19:17:33', NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ruang_dipesan`
--

CREATE TABLE IF NOT EXISTS `ruang_dipesan` (
  `id` int(5) NOT NULL,
  `kode_ruangan` varchar(25) NOT NULL,
  `nama_pemesan` varchar(25) NOT NULL,
  `nama_ruangan` varchar(25) NOT NULL,
  `prodi_dosen` varchar(25) NOT NULL,
  `jam_mulai` varchar(25) NOT NULL,
  `durasi` varchar(25) NOT NULL,
  `aksi` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ruang_kosong`
--

CREATE TABLE IF NOT EXISTS `ruang_kosong` (
  `id` int(5) NOT NULL,
  `kode_ruangan` varchar(25) NOT NULL,
  `nama_ruangan` varchar(25) NOT NULL,
  `jam` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(5) NOT NULL,
  `nip_nim` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` enum('-','admin','mhs','dosen','superadmin') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nip_nim`, `nama`, `password`, `type`) VALUES
(1, '1402120100', 'riski', '', '-'),
(2, '-', 'admin', '4297f44b13955235245b2497399d7a93', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pemesan_ruangan`
--
ALTER TABLE `pemesan_ruangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ruang_b_tersedia`
--
ALTER TABLE `ruang_b_tersedia`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indexes for table `ruang_dipesan`
--
ALTER TABLE `ruang_dipesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ruang_kosong`
--
ALTER TABLE `ruang_kosong`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pemesan_ruangan`
--
ALTER TABLE `pemesan_ruangan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ruang_b_tersedia`
--
ALTER TABLE `ruang_b_tersedia`
  MODIFY `id_ruangan` smallint(4) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=311;
--
-- AUTO_INCREMENT for table `ruang_dipesan`
--
ALTER TABLE `ruang_dipesan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ruang_kosong`
--
ALTER TABLE `ruang_kosong`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
