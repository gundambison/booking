<html>
    <?php
        include"header.php";
        include"navbar.php";
    ?>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <label>Tempat</label><br>
                    <label><a href="">Gedung Tarbiayah A</a></label><br>
                    <label><a href="">Gedung Tarbiayah B</a></label><br>
                    <label><a href="">Gedung Tarbiayah C</a></label>
                </div>
                <div class="col-md-4">
                    <div class="container">
                        <div class="row">
                            <div class="span12">
                                <table class="table-condensed table-bordered table-striped">
                                    <thead>
                                        <tr>
                                          <th colspan="7">
                                            <span class="btn-group">
                                                <a class="btn"><i class="icon-chevron-left"></i></a>
                                                <a class="btn active">February 2012</a>
                                                <a class="btn"><i class="icon-chevron-right"></i></a>
                                            </span>
                                          </th>
                                        </tr>
                                        <tr>
                                            <th>Su</th>
                                            <th>Mo</th>
                                            <th>Tu</th>
                                            <th>We</th>
                                            <th>Th</th>
                                            <th>Fr</th>
                                            <th>Sa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="muted">29</td>
                                            <td class="muted">30</td>
                                            <td class="muted">31</td>
                                            <td>1</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>6</td>
                                            <td>7</td>
                                            <td>8</td>
                                            <td>9</td>
                                            <td>10</td>
                                            <td>11</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>13</td>
                                            <td>14</td>
                                            <td>15</td>
                                            <td>16</td>
                                            <td>17</td>
                                            <td>18</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td class="btn-primary"><strong>20</strong></td>
                                            <td>21</td>
                                            <td>22</td>
                                            <td>23</td>
                                            <td>24</td>
                                            <td>25</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>27</td>
                                            <td>28</td>
                                            <td>29</td>
                                            <td class="muted">1</td>
                                            <td class="muted">2</td>
                                            <td class="muted">3</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                      <thead>
                        <tr style="background-color : #74b9ff">
                          <th scope="col">Periode</th>
                          <th scope="col">FTK - 01 (B)</th>
                          <th scope="col">FTK - 02 (B)</th>
                          <th scope="col">FTK - 03 (B)</th>
                          <th scope="col">FTK - 04 (B)</th>
                          <th scope="col">FTK - 05 (B)</th>
                          <th scope="col">FTK - 06 (B)</th>
                          <th scope="col">FTK - 07 (B)</th>
                          <th scope="col">FTK - 08 (B)</th>
                          <th scope="col">FTK - 09 (B)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">08:00 - 09:30</th>
                          <td></td>
                          <td rowspan="2" style="background-color : #55efc4">PTI - Bustomi</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">09:30 - 10:30</th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td rowspan="3" style="background-color : #55efc4">BK - Sri Riski</td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">08:00 - 09:30</th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td rowspan="2" style="background-color : #55efc4">PBI - Marzuki</td>
                          <td></td>
                          <td></td>
                          <td rowspan="4" style="background-color : #55efc4">PBA - Nazaruddin</td>
                        </tr>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">08:00 - 09:30</th>
                          <td rowspan="2" style="background-color : #55efc4">PTE - Ghufran Ibnu Yosa</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">08:00 - 09:30</th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr>
                          <th scope="row" style="background-color : #74b9ff">08:00 - 09:30</th>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
    
    
    
</html>