<?php 
include "koneksi.php";
?>
<html>
    <?php 
        include "header.php";
        include "navbar.php";
    ?>
    <body style="background-image: url("img/latar.jpg");">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Please Log in</h2>
                    <hr>
                    <form  action="login_process.php" method="post">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Username</label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="username">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <label>Password</label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                
                            </div>
                            <div class="col-md-1">
                                
                            </div>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-primary" name="login" value="Log in">
                            </div>
                        </div>
                    </form>
                        <hr>
                    <!--<table>
                        <tr>
                            <th><label> Username :</label></th>
                            <th>
                                <div class="form-group">
                                <input type="text" name="username">
                                </div>    
                            </th>
                        </tr>
                        <tr>
                            <th><label> Password :</label></th>
                            <th><input type="password" name="password"></th>
                        </tr>
                        
                    
                    </table>-->
                </div>
                <div class="col-md-6">    
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6">    
                </div>
                <div class="col-md-6" style="background-color : #74b9ff; padding: 15px;">    
                <label>Informasi</label>
                <br>
                <label>User yang dapat menggunakan layanan ini adalah Mahasiswa,</label>
                <label>Dosen dan Staff kampus, dan pastikan saat anda login isikan</label>
                <label>Username dan Password dengan benar. Jika anda adalah :</label>
                <label> 1. Mahasiswa, Maka gunakan NIM sebagai Username </label>
                <label> 2. Dosen atau Staff E-mail sebagai Username</label>
                <label> Bagi user masih menggunakan password default, di sarankan</label>
                <label> melakukan perubahan password</label>
                </div>
            </div>
        </div>
    </body>
</html>