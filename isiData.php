<html>
    <?php
        include "header.php";
        include "navbar.php";
    ?>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Isi Data Berikut :</h2>
                    <hr>
                    <form>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Nama </label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="nama">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Ruangan</label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="ruangan">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Prodi-Dosen </label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="prodiDosen">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Jam Mulai</label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" name="jam">
                                    <option>08 : 00 wib</option>
                                    <option>09 : 00 wib</option>
                                    <option>10 : 00 wib</option>
                                    <option>11 : 00 wib</option>
                                    <option>12 : 00 wib</option>
                                    <option>13 : 00 wib</option>
                                    <option>14 : 00 wib</option>
                                    <option>15 : 00 wib</option>
                                    <option>16 : 00 wib</option>
                                    <option>17 : 00 wib</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Durasi</label>
                            </div>
                            <div class="col-md-1">
                                <label>:</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" name="durasi">
                                    <option>1 SKS</option>
                                    <option>2 SKS</option>
                                    <option>3 SKS</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                
                            </div>
                            <div class="col-md-4">
                                
                            </div>
                            <div class="col-md-4">
                                <input type="button" class="btn btn-primary" name="login" value="Selesai">
                            </div>
                        </div>
                    </form>
                        <hr>
                    <!--<table>
                        <tr>
                            <th><label> Username :</label></th>
                            <th>
                                <div class="form-group">
                                <input type="text" name="username">
                                </div>    
                            </th>
                        </tr>
                        <tr>
                            <th><label> Password :</label></th>
                            <th><input type="password" name="password"></th>
                        </tr>
                        
                    
                    </table>-->
                </div>
                <div class="col-md-6">    
                </div>
            </div>
            
        </div>
    </body>
    
</html>